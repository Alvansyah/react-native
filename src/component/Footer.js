import React, { Component } from 'react';
import { Footer, FooterTab, Button, Icon ,Text} from 'native-base';
import {StyleSheet} from 'react-native'
import { Actions } from 'react-native-router-flux';

export default class FooterTabsIconExample extends Component {
  render() {
    return (
        <Footer>
          <FooterTab style={{backgroundColor:'#fff', borderTopWidth:1, borderTopColor:'#039be5'}}>
            <Button
            vertical
            style={{backgroundColor:'#fff'}}
            onPress={()=>{
            if(!this.props.home)
               Actions.home()}
            }
            
            
            >
              <Icon 
               name="home"
               type="Entypo" 
               style={this.props.home? Styles.mainColorActive:Styles.mainColor}
               
               />
                <Text style={this.props.home? Styles.mainColorActive:Styles.mainColor}>Home</Text>
            </Button>
            <Button
            vertical
            style={{backgroundColor:'#fff'}}
            onPress={()=>{
            if(!this.props.cart)
               Actions.cart()}
            }
            
            
            >
              <Icon 
               name="shopping-cart"
               type="Entypo"
               style={this.props.cart? Styles.mainColorActive:Styles.mainColor}
               
               />
                <Text style={this.props.cart? Styles.mainColorActive:Styles.mainColor}>Cart</Text>
            </Button>
            <Button
            vertical
            style={{backgroundColor:'#fff'}}
            onPress={()=>{if(!this.props.account)
               Actions.account()}}
            >
              <Icon name="account" 
                type="MaterialCommunityIcons" 
                style={this.props.account? Styles.mainColorActive:Styles.mainColor}
              />
                <Text style={this.props.account? Styles.mainColorActive:Styles.mainColor}>Account</Text>
            </Button>
          </FooterTab>
        </Footer>
    );
  }
}

const Styles = StyleSheet.create({
    mainColor:{
        color:'#bdbdbd'
    },
    mainColorActive:{
        color:'#26c6da'
    },
})

































































































































































































































































































































































































































































