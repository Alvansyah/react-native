import React, { Component} from "react";
import {Text, 
    View, 
    TouchableOpacity, 
    StyleSheet} from 'react-native'
import { Actions } from "react-native-router-flux";
import { Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title } from 'native-base';
import { connect } from "react-redux";
import {doLogout} from '../modules/AppState'
    
class HeaderCustomView extends Component{

    logout(){
        this.props.doLogout()
        if(!this.props.isLogin){
            Actions.login();
        }
    }

    left(){
        if(this.props.back){
            return (
                <Left>
                    <Button
                        transparent
                        onPress={()=> Actions.pop()}
                        >
                            <Icon name='arrow-back' />
                    </Button>
                </Left>
            )  
        }
    }

    right(){
        if(this.props.logout){
            return (
                <Button
                transparent
                onPress={()=> this.logout()}
                >
                    <Text style={{color:'#fff'}}> Logout</Text>
                </Button>
            )
        }
    }





    render(){
        return(
            <Header style={styles.header}>
                    {this.left()}                    
                <Body style={{marginLeft:this.props.back?0:10}}> 
                    <Title>{this.props.title? this.props.title : 'Tittle'}</Title>
                </Body>
                <Right>
                {this.right()}
                </Right>
            </Header>
        )
    }
}


const styles = StyleSheet.create({
    header:{
        backgroundColor: '#fb8c00'
    }
})

const HeaderCustom = connect(
    state => ({
        isLogin: state.app.isLogin,
    }),
    dispatch => ({
        doLogout: () => dispatch(doLogout()),
    }),
)
(HeaderCustomView)

export default HeaderCustom;