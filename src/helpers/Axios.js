const axios = require('axios');

axios.defaults.baseURL = 'http://verywell.lux.my.id';
axios.defaults.timeout = 2000;
export default axios;