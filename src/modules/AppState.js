import Axios from '../helpers/Axios';
import { PermissionsAndroid, ToastAndroid } from 'react-native';

type AppStateType = {
    isConnected: boolean,
    phoneImei: string,
    phoneLocation: mixed,
    isLogin: boolean,
    loginLoading: boolean,
    currentUser: mixed,
    accessToken: string,
    updateLoading: boolean,
    buttonLoading:boolean,
    registerLoading:boolean,
};

type ActionType = {
    type: string,
    payload?: any,
};

//inisial state Global
export const initialState: AppStateType = {
    isConnected: true,
    phoneLocation: null,
    loginLoading: false,
    accessToken: null,
    isLogin: false,
    currentUser: null,
    updateLoading:false,
    registerLoading:false,
    about:null,
    aboutLoading:true,
};

//Actions
export const CHANGE_CONNECTION_STATUS = 'AppState/CHANGE_CONNECTION_STATUS';
export const SET_LOGIN_LOADING = 'AppState/SET_LOGIN_LOADING';
export const SET_ACCESS_TOKEN = 'AppState/SET_ACCESS_TOKEN';
export const SET_LOGIN = 'AppState/SET_LOGIN';
export const SET_LOGOUT = 'AppState/SET_LOGOUT';
export const SET_UPDATE_LOADING = 'AppState/SET_UPDATE_LOADING';
export const SET_BUTTON_LOADING = 'AppState/SET_BUTTON_LOADING';
export const SET_REGISTER_LOADING = 'AppState/SET_REGISTER_LOADING';

//Actions Creator
export function connectionState(status): ActionType {
    return {
        type: CHANGE_CONNECTION_STATUS,
        isConnected: status,
    };
}

export function setAccessToken(accessToken): ActionType {
    return {
        type: SET_ACCESS_TOKEN,
        accessToken,
    };
}

export function setLogin(payload): ActionType {
    return {
        type: SET_LOGIN,
        payload,
    };
}

export function setLoginLoading(loginLoading): ActionType {
    return {
        type: SET_LOGIN_LOADING,
        loginLoading
    };
}

export function setButtonLoading(buttonLoading): ActionType {
    return {
        type: SET_BUTTON_LOADING,
        buttonLoading
    };
}

export function setUpdateLoading(loading) {
    return {
        type: SET_UPDATE_LOADING,
        loading
    };
}

export function setRegisterLoading(loading) {
    return {
        type: SET_REGISTER_LOADING,
        loading
    };
}

export function getApplicationToken() {
    console.log('getApplicationToken');

    return (dispatch, getState) => {
        const state = getState();
        const isConnected = state.app.isConnected;

        if (!isConnected) {
        return ToastAndroid.show(
            'Anda sedang offline, silahkan periksa setelan koneksi',
            ToastAndroid.SHORT,
        );
        }

        let GetAppTokenRequest = {
            // url: Config.API_URL+'/api/oauth/token',
            url: '/api/oauth/token',
            data: {
                grant_type: 'client_credentials',
                client_id: Config.CLIENT_ID,
                client_secret: Config.CLIENT_SECRET
            },
        };
        console.log('GetAppTokenRequest = ', GetAppTokenRequest);

        Axios.post(GetAppTokenRequest.url, GetAppTokenRequest.data)
        .then(response => {
            const data = response.data;
            dispatch(setAccessToken(data.access_token));
        })
        .catch(error => {
            console.log(error)
            dispatch(setLoginLoading(false));
            return ToastAndroid.show('Login gagal, email/password tidak ditemukan', ToastAndroid.SHORT);
            showError(error, 'login');
        });
  };
}


//Function
export function registerUser(data) {

    return (dispatch, getState) => {

        dispatch(setRegisterLoading(true));

        dispatch(getApplicationToken());
        const state = getState();
        let registerRequest = {
            url: '/api/user/register',
            data: data,
            config: {
                Headers: {
                    Authorization: `Bearer ${state.app.accessToken}`,
                    "Content-Type":"application/json",
                    "Accept":"application/json"
                }
            }
        };
        console.log('registerData = ', registerRequest);

        Axios.post(
                registerRequest.url,
                registerRequest.data,
                registerRequest.config
            )
            .then(response => {
                dispatch(checkLogin({username: data.email, password: data.password}));
                dispatch(setRegisterLoading(false));
                return ToastAndroid.show('Daftar Berhasil', ToastAndroid.SHORT);
            })
            .catch(error => {
                // alert(error);
                dispatch(setRegisterLoading(false));

                if(error.errors.email && error.errors.mobile_number){
                    return ToastAndroid.show('Email dan nomor sudah dipakai', ToastAndroid.SHORT);
                }
                else if(error.errors.email){
                    return ToastAndroid.show('Email sudah dipakai', ToastAndroid.SHORT);

                }
                else if(error.errors.mobile_number){
                    return ToastAndroid.show('Nomor telepon sudah dipakai', ToastAndroid.SHORT);
                }
                else{
                    showError(error, 'register');
                }
            });
    }

}


//Function
//Harus ada Dispatch dan GetState
export function checkLogin(payload) {
    console.log('checkLogin', payload);

    return (dispatch, getState) => {
        const state = getState();
        const username = payload.username;
        const password = payload.password;

        if (username === '') {
            return ToastAndroid.show('Username Tidak Boleh Kosong', ToastAndroid.SHORT);
        }

        if (password === '') {
            return ToastAndroid.show('Password Tidak Boleh Kosong', ToastAndroid.SHORT);
        }


        let loginRequest = {
            method: 'post',
            url: '/api/oauth/token',
            data: {
                client_id: 2,
                client_secret: 'VoiHW0I8LDgCLQljHxWNgmSJJur58dfZVpMXt04s',
                grant_type: 'password',
                username: username,
                password: password,
                scope: '*',
            },
            params:{}
        };
        console.log('loginData = ', loginRequest);
        dispatch(setLoginLoading(true));
    
        Axios(loginRequest)
        .then(response => {
            const data = response.data;
            dispatch(setAccessToken(data.access_token));
            dispatch(getCurrentUser());
            // dispatch(setButtonLoading(false));
        })
        .catch(error => {
            console.log('ERROR >>',error)
            dispatch(setLoginLoading(false));
            return ToastAndroid.show('Login gagal, email/password tidak ditemukan', ToastAndroid.SHORT);
            showError(error, 'login');
        });
    };
}

export function getCurrentUser() {
    return (dispatch, getState) => {
        const state = getState();
        // dispatch(setLoginLoading(true));
        let getUserRequest = {
            url: '/api/user',
            config: {
                headers: {Authorization: `Bearer ${state.app.accessToken}`},
            },
        };
        console.log('getUserRequest = ', getUserRequest);
    
    Axios
      .get(getUserRequest.url, getUserRequest.config)
      .then(response => {
        const data = response.data;
        console.log(data);
        dispatch(
          setLogin({
            isLogin: true,
            currentUser: data,
          }),
        );
        dispatch(setLoginLoading(false));
      })
      .catch(error => {
        dispatch(setLoginLoading(false));
        showError(error, 'getCurrentUser');
      });
    };
}

export function doLogout(): ActionType {
    return {
        type: SET_LOGOUT,
    };
}

export function updateProfile(payload) {
    return (dispatch, getState) => {
        dispatch(setUpdateLoading(true));
        var state = getState();

        var dataUser = state.app.currentUser
        if(dataUser==null){
          return dispatch(setUpdateLoading(false));
        }

        let updateProfileRequest = {
            url: `/api/user/update`,
            method:'patch',
            params: {},
            headers: {
                Authorization: `Bearer ${state.app.accessToken}`,
                "Content-Type":"application/json",
                "Accept":"application/json"
            },
            data: payload
        };

        console.log('updateProfileRequest = ', updateProfileRequest);
        Axios(updateProfileRequest)
            .then(response => {
                dispatch(getCurrentUser());
                dispatch(setUpdateLoading(false));
                return ToastAndroid.show('Profil Berhasil diupdate', ToastAndroid.SHORT);
            })
            .catch(error => {
                dispatch(setUpdateLoading(false));
                showError(error, 'Update Profile');
            });

    };
}

export default function AppStateReducer(
    state: AppStateType = initialState,
    action: ActionType,
): AppStateType {
    switch (action.type) {
        case CHANGE_CONNECTION_STATUS:
            return {
                ...state,
                isConnected: action.isConnected,
            };
        case SET_LOGIN_LOADING:
            return {
                ...state,
                loginLoading: action.loginLoading,
            };
        case SET_BUTTON_LOADING:
            return {
                ...state,
                buttonLoading: action.buttonLoading,
            };
        case SET_UPDATE_LOADING:
            return {
                ...state,
                updateLoading : action.loading,
            };
        case SET_REGISTER_LOADING:
            return {
                ...state,
                registerLoading : action.loading,
            };
        case SET_ACCESS_TOKEN:
            return {
                ...state,
                accessToken: action.accessToken,
            };
        case SET_LOGIN:
            return Object.assign({}, state, {
                isLogin: action.payload.isLogin,
                currentUser: action.payload.currentUser,
            });
        case SET_LOGOUT:
            return Object.assign({}, state, {
                currentUser: false,
                accessToken: null,
                isLogin: null,
            });
            
        default:
            return state;
    }
}