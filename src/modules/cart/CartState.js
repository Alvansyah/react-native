// @flow
import Axios from '../../helpers/Axios'
import { PermissionsAndroid, ToastAndroid } from 'react-native';


//inisial state 
export const initialState = {
    loading: false,
    carts:[]
};

//actions
export const SET_CART_LOADED = 'CartState/SET_CART_LOADED';
export const SET_LOADING = 'CartState/SET_LOADING';
export const SET_NULL_CART = 'CartState/SET_NULL_CART';


//action creator
export function cartLoaded(carts){
    return {
        type: SET_CART_LOADED,
        carts,
    };
}

export function setLoading(loading){
    return {
        type: SET_LOADING,
        loading,
    };
}


export function setNullCart(){
    return {
        type: SET_NULL_CART,
    };
}

//function
export function addToCart(quantity,product, notes){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        // console.log("quantity",quantity)
        // console.log("product",product)

        
        const cart = state.cart.carts;

        cart.push({
            product: product,
            quantity : quantity,
            notes : notes
        });
        dispatch(cartLoaded(cart))
    };
}




export default function CartStateReducer(
    state = initialState, action = {}
){
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
        case SET_CART_LOADED:
            return {
                ...state,
                carts: action.carts,
                loading:false
            };
        case SET_NULL_CART:
            return {
            ...state,
            carts: [],
            };

            
        default:
            return state;
    }
}