import React, { Component} from "react";
import {Text, 
    View, 
    TouchableOpacity, 
    StyleSheet,
    Image,
    ActivityIndicator,
    FlatList,
} from 'react-native'
import { Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    FooterTab,     
    Thumbnail,
} from 'native-base';
import Head from '../../component/Head';
import {connect} from 'react-redux'
import NumberFormat from 'react-number-format';
import {setNullCart} from './CartState'
import Footer from '../../component/Footer';
import {Orders} from '../orders/OrdersState'


class ChartView extends Component {

    constructor(props){
        super(props)
        this.state ={
            total : 0,
            orders:[],
        
        }
    }

    onFinish(){
        this.props.Orders(this.state.orders)
    }
    
    componentDidMount(){
        var isi_cart = this.props.cart
        var total = 0

        isi_cart.map((item)=>
            total += item.product.master_price * item.quantity
        );
        
        this.setState({
            total : total, 
        });
        
        isi_cart.map ((item,key)=>{
            this.state.orders.push({
                product_id : item.product.id,
                quantity : item.quantity,
                notes : item.notes,

            })
        });
    }

    render(){
        // console.log('hahah',this.state.orders)
        return(
            <Container>
                <Head title={'Cart List'} back={true}/>
                <View style={{flex:1, padding:10, paddingHorizontal:10, paddingTop:10}}>
                    {this.props.loading?
                    <View style={{alignItems:'center', paddingTop:30}}>
                    <ActivityIndicator  color={'#64b5f6'} size='large'/>
                    </View>
                  :
                    <FlatList
                         data={this.props.cart}
                         bounces={false}
                         refreshing={this.props.loading}
                         showsVerticalScrollIndicator={false}
                         onRefresh={this.load}
                         numColumns={1}
                         keyExtractor={item => `cart-${item.id}`}
                         renderItem={({item, index}) => <Chartlist data={item} />}
                     />
                    }
                </View>
                <TouchableOpacity onPress={() => this.props.setNullCart()}>
                <Icon name="trash" style={{paddingLeft: 10,paddingRight:10, textAlign:'right'}} size={20} color="red" />
                </TouchableOpacity>
                <Text style={{textAlign:'right'}}>TOTAL BAYAR : 
                <NumberFormat
                    value={this.state.total}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp. '}
                    renderText={value => <Text style={{color:'#000000',fontWeight:'bold'}}>{value}</Text>}
                />
                </Text>
                <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={()=>this.onFinish()}
                            >                        
                            <Text style={Styles.buttonText}>Checkout</Text>
                </TouchableOpacity>
                <Footer cart />
            </Container>
            
        )
    }
}


const Chartlist =(props) => {
    return(
        <TouchableOpacity
            style={Styles.productsCard}
            
        >
            <Thumbnail
                source={{uri : props.data.product.image_path}}
                style={Styles.imageProducts}
                large
            />
            <View style={[Styles.flex1,{justifyContent:'center'}]}>
                <Text style={{fontSize:18}}>{props.data.product.name}</Text>
                <NumberFormat
                    value={props.data.product.master_price}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp. '}
                    renderText={value => <Text style={{color:'#f44336', fontWeight:'bold'}}>{value}</Text>}
                /> 
                <Text>Total Beli :{props.data.quantity}</Text>
                <Text>Notes : {props.data.notes}</Text>
            </View>
        </TouchableOpacity>
    )
}

const Styles = StyleSheet.create({
    flex1: {
        flex:1
    },
    imageProducts:{
        backgroundColor:'#cdcdcd',
        borderRadius:5,
        marginRight:10,
        width:100,
        height:100
    },
    productsCard:{
        flex:1,
        padding:10,
        borderRadius:20,
        marginBottom:15,
        backgroundColor:'#cdcdcd',
        flexDirection:'row',
        borderWidth:1,
    },
    buttonText:{
        marginTop:0,
        borderColor:'white',
        borderWidth:1,
        borderRadius:10,
        padding:12,
        backgroundColor:'#f57c00',
        textAlign:'center',
        marginLeft:200,
        fontWeight:'bold'
    },
})


const chartlist = connect(
  state => ({
    cart :state.cart.carts,
  }),
  dispatch => ({
    setNullCart: () => dispatch(setNullCart()),
    Orders: (data) => dispatch(Orders(data)),
    
  }),
)
(ChartView);


export default chartlist;