// @flow
import Axios from '../../helpers/Axios'
import { PermissionsAndroid, ToastAndroid } from 'react-native';

//inisial state 
export const initialState = {
    loading: false,
    products: [],
    category:[],
    detail:{},
    product:{}
};

//actions
export const SET_PRODUCTS_LOADED = 'AppState/SET_PRODUCTS_LOADED';
export const SET_LOADING = 'AppState/SET_LOADING';
export const SET_CATEGORY_LOADED = 'AppState/SET_CATEGORY_LOADED';
export const SET_DETAIL_LOADED = 'AppState/SET_DETAIL_LOADED';
export const SET_PRODUCT_LOADED = 'AppState/SET_PRODUCT_LOADED';


//action creator
export function productsLoaded(products) {
    return {
        type: SET_PRODUCTS_LOADED,
        products,
    };
}

export function categoryLoaded(category) {
    return {
        type: SET_CATEGORY_LOADED,
        category,
    };
}

export function setLoading(loading) {
    return {
        type: SET_LOADING,
        loading,
    };
}

export function setDetail(detail) {
    return {
        type: SET_DETAIL_LOADED,
        detail,
    };
}

export function productLoaded(product) {
    return {
        type: SET_PRODUCT_LOADED,
        product,
    };
}


//function
export function loadProducts(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        var productid = productid
        dispatch(setLoading(true));

        let productsRequest = {
            method: 'get',
            url: '/api/product/products/',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('productsRequest = ', productsRequest);
    
        Axios(productsRequest)
            .then(response => {
                const data = response.data.data;
                dispatch(productsLoaded(data));
            })
            .catch(error => {
            });
    };
}

export function detail(id){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        var productid = id
        dispatch(setLoading(true));

        let detailRequest = {
            method: 'get',
            url: `/api/product/products/${productid}`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('detailRequest = ', detailRequest);
    
        Axios(detailRequest)
            .then(response => {
                const data = response.data.data;
                dispatch(productLoaded(data));
            })
            .catch(error => {                                                                                                          
            });
    };
}

export function loadCategory(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let categoryRequest = {
            method: 'get',
            url: '/api/product/categories/',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('categoryRequest = ', categoryRequest);
    
        Axios(categoryRequest)
            .then(response => {
                const data = response.data.data;
                dispatch(categoryLoaded(data));
            })
            .catch(error => {
            });
    };
}

export function loadDetail(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let detailRequest = {
            method: 'get',
            url: '/api/product/categories/',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('detailRequest = ', detailRequest);
    
        Axios(detailRequest)
            .then(response => {
                const data = response.data.data;
                dispatch(detailLoaded(data));
            })
            .catch(error => {
            });
    };
}


export default function HomeStateReducer(state = initialState, action = {}) {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
        case SET_PRODUCTS_LOADED:
            return {
                ...state,
                products: action.products,
                loading:false
            };
        case SET_CATEGORY_LOADED:
             return {
                ...state,
                category: action.category,
                loading:false
            };
            case SET_DETAIL_LOADED:
                return {
                   ...state,
                   detail: action.detail,
                   loading:false
               };
             case SET_PRODUCT_LOADED:
                return {
                   ...state,
                   product: action.product,
                   loading:false
               };
        default:
            return state;
    }
}