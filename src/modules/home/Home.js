import React, { Component} from "react";
import {Text, 
    View, 
    TouchableOpacity, 
    StyleSheet,
    Image,
    ActivityIndicator,
    FlatList,
} from 'react-native'
import { Actions } from "react-native-router-flux";
import { Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    FooterTab,     
    Thumbnail,
} from 'native-base';
import Head from '../../component/Head';
import {connect} from 'react-redux'
import {loadProducts} from './HomeState'
import {loadCategory} from './HomeState'
import Footer from '../../component/Footer';
import NumberFormat from 'react-number-format';


class HomeView extends Component{

    constructor(props){
        super(props)
        this.load=this.load.bind(this);
    }
    
    componentDidMount(){
        this.load();
    }

    load(){
        this.props.loadProducts();
        this.props.loadCategory();
    }

    head(){
        return(
            <View>
                <Image 
                source={require('../../asset/ambarr.jpg')}
                style={{width:'100%', height:200, borderRadius:5,marginBottom:10, marginTop:10}}
                />
                <View Style={Styles.imageCategory}>
                    <FlatList
                         data={this.props.category}
                         bounces={false}
                         horizontal={true}
                         showsHorizontalScrollIndicator={false}
                         numColumns={1}
                         showsVerticalScrollIndicator={false}
                         keyExtractor={item => `category-${item.id}`}
                         renderItem={({item, index}) => <CategoryCard data={item} />}
                     />
                </View>
            </View>
        )
    }



    render(){
        return(
            <Container>
                <Head title={'Home'} logout={true}/>
                <View style={{flex:1, padding:10, paddingHorizontal:10, paddingTop:10}}>
                    {this.props.loading?
                    <View style={{alignItems:'center', paddingTop:30}}>
                    <ActivityIndicator  color={'#64b5f6'} size='large'/>
                    </View>
                  :
                    <FlatList
                         data={this.props.products}
                         bounces={false}
                         refreshing={this.props.loading}
                         showsVerticalScrollIndicator={false}
                         onRefresh={this.load}
                         ListHeaderComponent={() => this.head()}
                         numColumns={1}
                         keyExtractor={item => `products-${item.id}`}
                         renderItem={({item, index}) => <ProductsCardFlex data={item} />}
                     />
                    }
                </View>
                <Footer home />
            </Container>  
        )
    }
}


const CategoryCard =(props) => {
    return(
        <TouchableOpacity
            style={Styles.CategoryCard}
            activeOpacity={0.9}
        >
            <Thumbnail
                source={{uri : props.data.image_path}}
                style={Styles.imageProducts}
                
            />
        
            <View style={[Styles.flex,{justifyContent:'center'}]}>
                <Text style={{fontSize:15,paddingLeft:5,marginBottom:20}}>{props.data.name}</Text>
            </View>
        </TouchableOpacity>
    )
}


const ProductsCardFlex =(props) => {
    return(
        <TouchableOpacity
            style={Styles.productsCard}
            onPress={()=>Actions.detail({productid: props.data.id})}
            
        >
            <Thumbnail
                source={{uri : props.data.image_path}}
                style={Styles.imageProducts}
                large
            />
            <View style={[Styles.flex1,{justifyContent:'center'}]}>
                <Text style={{fontSize:18}}>{props.data.name}</Text>
                <NumberFormat
                    value={props.data.master_price}
                    displayType={'text'}
                    thousandSeparator={true}
                    prefix={'Rp. '}
                    renderText={value => <Text style={{color:'#f44336', fontWeight:'bold'}}>{value}</Text>}
                /> 
            </View>
        </TouchableOpacity>
    )
}

const Styles = StyleSheet.create({
    flex1: {
        flex:1
    },
    imageProducts:{
        backgroundColor:'#cdcdcd',
        borderRadius:5,
        marginRight:10,
        width:100,
        height:100
    },
    productsCard:{
        flex:1,
        padding:10,
        borderRadius:20,
        marginBottom:15,
        backgroundColor:'#cdcdcd',
        flexDirection:'row',
        borderWidth:1,
    }
})

const Home = connect(
    state => ({
        loading: state.home.loading,
        products: state.home.products,
        isLogin: state.app.isLogin,
        category: state.home.category,

    }),
    dispatch => ({
        loadProducts: () => dispatch(loadProducts()),
        loadCategory: () => dispatch(loadCategory()),
    }),
)
(HomeView)

export default Home;
