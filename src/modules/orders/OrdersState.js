// @flow
import Axios from '../../helpers/Axios'
import { PermissionsAndroid, ToastAndroid } from 'react-native';
import { Actions } from 'react-native-router-flux';

//inisial state 
export const initialState = {
    loading: false,
    dataOrder: [],
    shipping: [],
    idOrder: {},
    dataSaveWarehouse: {},
    dataSavePayment: {},
    GetDataPayment: [],
    dataSaveShipping: {},
    GetDataShipping: [],
    warehouse: [],
    makePayment:[]
};

//actions
export const SET_LOADING = 'AppState/SET_LOADING';
export const SET_PRODUCTS_LOADED = 'AppState/SET_PRODUCTS_LOADED';
export const SET_ORDERS = 'AppState/SET_ORDERS';
export const SET_IDORDER = 'AppState/SET_IDORDER';
export const SET_GETSHIPPING = 'AppState/SET_GETSHIPPING';
export const SET_WAREHOUSE = 'AppState/SET_WAREHOUSE';
export const SET_DATASAVEPAYMENT = 'AppState/SET_DATASAVEPAYMENT';
export const SET_DATAPAYMENT = 'AppState/SET_DATAPAYMENT';
export const SET_DATASAVESHIPPING = 'AppState/SET_DATASAVESHIPPING';
export const SET_DATASHIPPING = 'AppState/SET_DATASHIPPING';
export const SET_SAVEWAREHOUSE = 'AppState/SET_SAVEWAREHOUSE';
export const SET_MAKEPAYMENT = 'AppState/SET_MAKEPAYMENT';

//action creator
export function setLoading(loading) {
    return {
        type: SET_LOADING,
        loading,
    };
}

export function productsLoaded(products) {
    return {
        type: SET_PRODUCTS_LOADED,
        products,
    };
}

export function toOrder(data) {
    return {
        type: SET_ORDERS,
        data,
    };
}

export function toShipping(data) {
    return {
        type: SET_ORDERS,
        data,
    };
}


export function setIdOrder(id) {
    return {
        type: SET_IDORDER,
        id,
    };
}

export function setWarehouse(data) {
    return {
        type: SET_WAREHOUSE,
        data,
    };
}

export function setPayment(data) {
    return {
        type: SET_MAKEPAYMENT,
        data,
    };
}


export function setSaveWarehouse(data) {
    return {
        type: SET_SAVEWAREHOUSE,
        data,
    };
}

export function setDataShipping(data) {
    return {
        type: SET_DATASHIPPING,
        data,
    };
}


export function setSaveDataShipping(data) {
    return {
        type: SET_DATASAVESHIPPING,
        data,
    };
}


export function savePayment(data){
    return (dispatch, getState) => {
        
        dispatch(setLoading(true));
        
        dispatch(setSaveDataPayment(data));
        ToastAndroid.show('Success Choose ' + data.name, ToastAndroid.SHORT);
    };
}

export function setDataPayment(data) {
    return {
        type: SET_DATAPAYMENT,
        data,
    };
}
export function setSaveDataPayment(data) {
    return {
        type: SET_DATASAVEPAYMENT,
        data,
    };
}


export function choosePayment(){
    
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));

        let PaymentRequest = {
            method: 'get',
            url: '/api/payment_methods/',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('PaymentRequest = ', PaymentRequest);
    
        Axios(PaymentRequest)
            .then(response => {
                const data = response.data.data;
                console.log("Cek Payment", response.data.data);
                
                dispatch(setDataPayment(data));
            })
            .catch(error => {
                
            });
    };
}

export function saveShipping(data){
    return (dispatch, getState) => {

        dispatch(setLoading(true));

        dispatch(setSaveDataShipping(data));
        ToastAndroid.show('Success Shipping', ToastAndroid.SHORT);
    };
}

export function saveWarehouse(country){
    return (dispatch, getState) => {

        dispatch(setLoading(true));

        dispatch(setSaveWarehouse(country));
        console.log("cekkk", country);
        
    };
}

export function DataShipping(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));

        let ShippingRequest = {
            method: 'get',
            url: '/api/shipping_methods/',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('ShippingRequest = ', ShippingRequest);
    
        Axios(ShippingRequest)
            .then(response => {
                const data = response.data.data;
                
                dispatch(setDataShipping(data));
            })
            .catch(error => {
                
            });
    };
}

export function getWarehouse(data,page){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));

        let WarehouseRequest = {
            method: 'get',
            url: '/api/warehouses',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
            params: {
                "keyword":`${data}`,
                "page":page
                //"per_page":5,
            }
        };
        console.log('WarehouseRequest = ', WarehouseRequest);
    
        Axios(WarehouseRequest)
            .then(response => {
                const data = response.data.data;

                dispatch(setWarehouse(data));
            })
            .catch(error => {
                
            });
    };
}

export function getShipping(id){
    
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));

        let ShippingRequest = {
            method: 'post',
            url: `/api/user/orders/${id}/shipping`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
            },
        };
        console.log('ShippingRequest = ', ShippingRequest);
    
        Axios(ShippingRequest)
            .then(response => {
                const data = response.data.data;

                console.log("shiping", response.data.data)
                
                dispatch(toShipping(data));
            })
            .catch(error => {
                console.log("tesshipping");
                
            });
    };
}

export function Orders(cart,total){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));
        var cart = cart;

        let OrderRequest = {
            method: 'post',
            url: '/api/user/orders',
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
            },
            data: cart
        };
        console.log('State Order = ', OrderRequest);
    
        Axios(OrderRequest)
            .then(response => {
                const data = response.data.data;
                const idOrder = response.data.data.id;
                console.log("dataorder", response.data.data.id);
                
                dispatch(setIdOrder(idOrder));
                dispatch(toOrder(data));
                ToastAndroid.show('Success Order', ToastAndroid.SHORT);
                Actions.orders({order: cart, total: total})
            })
            .catch(error => {
               ToastAndroid.show('Failed Order', ToastAndroid.SHORT);
            });
    };
}


export function Warehouse(data){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));
        let WarehouseRequest = {
            method: 'post',
            url: '/api/user/orders/19/update',
            headers: {
                'Authorization': `Bearer {{user_access_token}}`,
                "Content-Type": 'application/json',
            },
            data: data
        };
        console.log('State Order = ', WarehouseRequest);
    
        Axios(WarehouseRequest)
            .then(response => {
                const data = response.data.data;
                dispatch(toOrder(data));
                ToastAndroid.show('Succes Payment', ToastAndroid.SHORT);
                // Actions.orders({order: cart})
            })
            .catch(error => {
               ToastAndroid.show('Failed Payment', ToastAndroid.SHORT);
            });
    };
}


export function getPayment(data){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken

        dispatch(setLoading(true));
        let getPaymentRequest = {
            method: 'post',
            url: '/api/user/orders/19/make_payment',
            headers: {
                'Authorization': `Bearer {{user_access_token}}`,
                "Content-Type": 'application/json',
            },
            data: data
        };
        console.log('State Order = ', getPaymentRequest);
    
        Axios(getPaymentRequest)
            .then(response => {
                const data = response.data.data;
                dispatch(toOrder(data));
                ToastAndroid.show('Succes Payment haha', ToastAndroid.SHORT);
                // Actions.orders({order: cart})
            })
            .catch(error => {
               ToastAndroid.show('Failed Payment nooooo', ToastAndroid.SHORT);
            });
    };
}

export default function OrderStateReducer(state = initialState, action = {}) {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
        case SET_ORDERS:
            return {
                ...state,
                dataOrder: action.data,
                loading:false
            };
            case SET_GETSHIPPING:
                return {
                    ...state,
                    shipping: action.data,
                    loading:false
                };
            case SET_IDORDER:
                return {
                    ...state,
                    idOrder: action.id,
                    loading:false
                };
            case SET_WAREHOUSE:
                return {
                    ...state,
                    warehouse: action.data,
                    loading:false
                };
            case SET_SAVEWAREHOUSE:
                return {
                    ...state,
                    dataSaveWarehouse: action.data,
                    loading:false
                };
            case SET_DATASHIPPING:
                return {
                    ...state,
                    GetDataShipping: action.data,
                    loading:false
                };
            case SET_DATASAVESHIPPING:
                return {
                    ...state,
                    dataSaveShipping: action.data,
                    loading:false
                };
            case SET_DATAPAYMENT:
                return {
                    ...state,
                    GetDataPayment: action.data,
                    loading:false
                };
            case SET_DATASAVEPAYMENT:
                return {
                    ...state,
                    dataSavePayment: action.data,
                    loading:false
                };
            case SET_MAKEPAYMENT:
                return {
                ...state,
                    makePayment: action.data,
                    loading:false
                };
        default:
            return state;
    }
}