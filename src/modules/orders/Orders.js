import React, { Component} from "react";
import {Text, 
    View, 
    TouchableOpacity, 
    StyleSheet,
    Image,
    ActivityIndicator,
    FlatList,ScrollView
} from 'react-native'
import { Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button,
    Icon, 
    Title,
    Content,
    FooterTab,     
Thumbnail,
Item,

} from 'native-base';
import {
    Orders,
    getShipping,
    getWarehouse,
    DataShipping,
    saveShipping,
    choosePayment,
    savePayment,
    getPayment
} from './OrdersState';
import Modal from 'react-native-modal';
import Head from '../../component/Head';
import {connect} from 'react-redux'
import { Actions } from 'react-native-router-flux';
import NumberFormat from 'react-number-format';

class OrderView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            alias: null,
            address: null,
            mobile_number: null,
            recipient_name: null,
            subdistrict: null,
            zip_code: null,
            modalVisible: false,
            modalPaymentVisible: false
        };
    }

    componentDidMount(){
        
        this.props.getWarehouse()
        console.log('PAYYYY',this.props.getPayment())
        
        this.setState({
            alias: this.props.dataAddress[0].alias,
            address: this.props.dataAddress[0].address,
            mobile_number: this.props.dataAddress[0].mobile_number,
            recipient_name: this.props.dataAddress[0].recipient_name,
            subdistrict: this.props.dataAddress[0].subdistrict.full_name,
            zip_code: this.props.dataAddress[0].zip_code
        })
        
    }

    setVisible(visible,id){
        
        this.setState({ modalVisible: visible });
        this.props.DataShipping()
    }

    setVisiblePayment(visible){
        
        this.setState({ modalPaymentVisible: visible });
        this.props.choosePayment()
    }

    render(){
        // console.log('hahaha', this.props.dataChoose);
        
        return (
            <Container>
                <Head title={'Order'} back={true} />
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                    <View style={styles.container}>
                        <View style={[styles.addressCard,]}>
                            <View style={{flexDirection: 'row', marginBottom: 12}}>
                                <TouchableOpacity
                                    activeOpacity={0.8}                                    
                                    onPress={() => Actions.chooseAddress()}
                                >
                                    <Text style={{color:'#000000'}}>Chooses Address</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{borderTopWidth: 2, borderTopColor: 'black'}} />
                            <View style={{flexDirection: 'column', marginTop: 10}}>
                                <Text style={{color:'white', fontWeight:'bold'}}>{this.state.alias}</Text>
                                <Text style={{color:'white'}}>{ this.state.recipient_name + " ("+this.state.mobile_number+')'}</Text>
                                <Text style={{color:'white'}}>{ this.state.address + this.state.subdistrict}</Text>
                                <Text style={{color:'white'}}>{this.state.zip_code}</Text>
                            </View>
                        </View>
                        <View style={styles.ProductCard}>
                            <FlatList
                                data= {this.props.orders}
                                bounces= {false}
                                //horizontal={true}
                                numColumns= {1}
                                showsVerticalScrollIndicator={false}
                                keyExtractor= {item => `product-${item.id}`}
                                renderItem= {({item, index}) => <OrderCard data={item} />}
                            />
                            <Modal 
                                isVisible={this.state.modalVisible}
                                onBackdropPress={() => {
                                    this.setState({ modalVisible: false });
                                  }}
                                style={styles.view}
                                swipeDirection={['up', 'left', 'right', 'down']} 
                                onSwipeComplete={this.close}  
                                backdropColor="#B4B3DB"
                                backdropOpacity={0.8}
                                animationIn="zoomInDown"
                                animationOut="zoomOutUp"
                                animationInTiming={600}
                                animationOutTiming={600}
                                backdropTransitionInTiming={600}
                                backdropTransitionOutT
                            >
                                <View style={{backgroundColor: 'white'}}>
                                  <FlatList
                                        data= {this.props.GetDataShipping}
                                        bounces= {false}
                                        //horizontal={true}
                                        numColumns= {1}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor= {item => `shiping-${item.id}`}
                                        renderItem= {({item, index}) => <ShippingCard data={item} saveShipping={(data) => this.props.saveShipping(data)}/>}
                                    />
                                </View>
                            </Modal>
                            <Modal 
                                isVisible={this.state.modalPaymentVisible}
                                onBackdropPress={() => {
                                    this.setState({ modalPaymentVisible: false });
                                  }}
                                style={styles.view}
                                
                            >
                                <View style={{backgroundColor: 'white'}}>
                                  <FlatList
                                        data= {this.props.GetDataPayment}
                                        bounces= {false}
                                        //horizontal={true}
                                        numColumns= {1}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor= {item => `shiping-${item.id}`}
                                        renderItem= {({item, index}) => <PaymentCard data={item} savePayment={(data) => this.props.savePayment(data)}/>}
                                    />
                                </View>
                            </Modal>
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity
                                    activeOpacity={0.8}                                    
                                    onPress={() => {
                                        this.setVisible(true,this.props.idOrder);
                                      }}
                                    style={{borderWidth:1, borderRadius: 5, borderColor:'#00adb5', backgroundColor: '#00adb5'}}
                                >
                                    <Text style={{ color:'white', fontWeight: 'bold', marginHorizontal: 10, marginVertical:5}}>Choose Shipping</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <TouchableOpacity
                                    activeOpacity={0.8}                                    
                                    onPress={() => {
                                        Actions.choosewarehouse();
                                      }}
                                    style={{borderWidth:1, borderRadius: 5, marginVertical:10, marginRight:10,  borderColor:'#00adb5', backgroundColor: '#00adb5'}}
                                >
                                    <Text style={{ color:'white', fontWeight: 'bold', marginHorizontal: 10, marginVertical:5}}>Choose Warehouse</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: 'row'}}>    
                                <Text style={{ color:'white', fontWeight: 'bold', left:240,}}>Sub Total</Text>
                                <NumberFormat
                                    value={this.state.total}
                                    displayType={'text'}
                                    thousandSeparator={true}
                                    prefix={'Rp. '}
                                    renderText={value => <Text style={{color:'#000000',fontWeight:'bold'}}>{value}</Text>}
                                />
                            </View>
                        </View>
                    </View>
                        </ScrollView>
            </Container>
        )
    }
}

const PaymentCard= (props) => {
    return (
        <View>
            <TouchableOpacity
                onPress={() => props.savePayment(props.data)}
                activeOpacity={0.5}
            >
                <View style={{flexDirection: 'row'}}>
                    <Thumbnail
                        source={{uri: props.data.logo_path}}
                        style={{borderRadius: 3}}
                        large
                    />
                    <View>
                        <Text style={{ marginLeft: 10, marginTop: 10}}>{props.data.name}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const ShippingCard= (props) => {
    return (
        <View>
            <TouchableOpacity
                onPress={() => props.saveShipping(props.data)}
                activeOpacity={0.5}
            >
                <View >
                    {/* <Thumbnail
                        source={{uri: props.data.logo_path}}
                        style={{borderRadius: 3}}
                        large
                    /> */}
                    <View>
                        <Text style={{ marginLeft: 10, marginTop: 10}}>{props.data.logistic.name}</Text>
                        <Text style={{ marginLeft: 10, marginBottom: 10}}>{props.data.description}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const OrderCard= (props) => {
    return (
        <View style={{ marginBottom: 20}}>
            <View style={{flexDirection: 'row',}}>
                <Thumbnail
                    source={{uri: props.data.product.image_path}}
                    style={{borderRadius: 3}}
                    large
                />
                <View>
                    <Text style={{color:'white', marginLeft: 10, marginTop: 10}}>{props.data.product.name}</Text>
                    <Text style={{color:'white', marginLeft: 10}}>{props.data.quantity} Items</Text>
                    <NumberFormat
                        style={{ color:'white', textAlign: 'right'}}
                        value={props.data.price * props.data.quantity}
                        displayType={'text'}
                        thousandSeparator={true}
                        prefix={'Rp. '}
                        renderText={value => <Text style={{marginLeft: 10, color:'white', fontSize: 15,}}>{value}</Text>}
                    />
                </View>
            </View>
        </View>
    )
}



const styles = StyleSheet.create({
    flex: {
        flex:1,
    },
    view: {
        justifyContent: 'flex-end',
        margin: 0,
        width: "100%",
    },
    modalView: {
        backgroundColor: "white",
        borderRadius: 4,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
    },
    buy: {
        borderWidth: 1,
        borderColor: 'black',
        borderRadius: 7,
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginVertical: 10,
    },
    cart: {
        flex:1,
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    addressCard: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 1,
        backgroundColor: '#fb8c00',
        borderWidth: 1,
        borderColor: 'white'
    },
    ProductCard: {
        padding: 10,
        borderRadius: 10,
        marginTop: 10,
        marginBottom: 10,
        backgroundColor: '#fb8c00',
        borderWidth: 1,
        borderColor: 'white'
    },
    container : {
        flex: 1,
        paddingRight: 10,
        paddingLeft: 10,
        backgroundColor: '#fafafa',
    },
    textBody: {
        color: 'white',
        fontSize: 10,
    },
    Footer: {
        backgroundColor: 'blue'
    }
})

const Order = connect(
    state => ({
        data: state.orders.data,
        dataAddress: state.account.dataAddress,
        dataChoose: state.account.dataChoose,
        dataOrder: state.orders.dataOrder,
        idOrder: state.orders.idOrder,
        warehouse: state.orders.warehouse,
        GetDataShipping: state.orders.GetDataShipping,
        GetDataPayment: state.orders.GetDataPayment,
        makePayment:state.orders.makePayment
    }),
    dispatch => ({
        Orders: () => dispatch(Orders()),
        getShipping: (id) => dispatch(getShipping(id)),
        savePayment: (data) => dispatch(savePayment(data)),
        DataShipping: () => dispatch(DataShipping()),
        getWarehouse: () => dispatch(getWarehouse()),
        choosePayment: () => dispatch(choosePayment()),
        saveShipping: (data) => dispatch(saveShipping(data)),
        getPayment: (data) => dispatch(getPayment(data)),
    }),
)
(OrderView);

export default Order;