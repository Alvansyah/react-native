import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput,
    Alert,
    ToastAndroid
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';
import {
    connect
} from 'react-redux';
import {
getWarehouse
} from '../account/AccountState';
import { Actions } from 'react-native-router-flux';
import { ScrollView } from 'react-native-gesture-handler';



class ChooseWarehouseViews  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            country:null,
            warehouse:null,
            page:0

        };
        this.setAddress = this.setAddress.bind(this)
    }


    setAddress(data){
        this.setState({
            country:data.full_name
        })
    }

    seacrhWarehouse(warehouse,page){
        this.setState({
            warehouse:warehouse,
            page:page
        })
        this.props.getWarehouse(this.state.warehouse, this.state.page)
    }

    addPage(){
        this.setState({
            page:this.state.page + 1
        })
    }

    minPage(){
        if(this.state.page == 0) {
            return ToastAndroid.show('No Data Address', ToastAndroid.SHORT)
        }
        this.setState({
            page:this.state.page -1
        })
        this.props.getWarehouse(this.state.address, this.state.page)
    }


    render(){
        return (
            <Container>
               <ScrollView>
                   <Head title={'Warehouse'} back={true}/>
                   <View style={styles.container}>
                       <View style={{alignSelf:'center', alignItems:'center'}}>
                            <Text>{this.props.subdistrict }</Text>
                            <View style={{flexDirection:'row',borderBottomColor:'#393e46'}}>
                                <TextInput
                                style={styles.TextInput}
                                onChangeText={text => this.seacrhWarehouse(text,1)}
                                />
                            </View>
                            <View>
                                <FlatList
                                    data={this.props.warehouse}
                                    bounces={false}
                                    numColumns={1}
                                    showsVerticalScrollIndicator={false}
                                    keyExtractor={item =>`warehouse-${item.id}`}
                                    renderItem={({item, index}) => <WarehouseCard data={item} />}
                                    />
                            <View style={{flexDirection:'row', alignSelf:'center'}}>
                                <TouchableOpacity
                                style={[styles.buttonpage,{marginRight:20}]}
                                activeOpacity={0.4}
                                onPress={()=> this.minPage()}
                                >
                                <View>
                                    <Text style={{color:'white', textAlign:'center'}}>Before</Text>
                                </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                style={[styles.buttonpage,{marginRight:20}]}
                                activeOpacity={0.4}
                                onPress={()=> this.addPage()}
                                >
                                <View>
                                    <Text style={{color:'white', textAlign:'center'}}>Next</Text>
                                </View>
                                </TouchableOpacity>
                            </View>
                                <TouchableOpacity
                                    style={[styles.buttonpage,{marginRight:20}]}
                                    activeOpacity={0.4}

                                    >
                                    <Text style={{color:'white', textAlign:'center'}}>Done</Text>
                                </TouchableOpacity>
                            </View>
                       </View>
                   </View>
               </ScrollView>
            </Container>
                )
            }
        }

        const WarehouseCard = (props) => {
            console.log('haha',props.data);
            
            return (
                <TouchableOpacity
                style={styles.districtsCard}
                    activeOpacity={0.8}    
                    onPress={() => props.setWarehouse(props.data)}
                >
                    <View>
                        <Text style={{color:'black'}}>{props.data.name}</Text>
                        <Text style={{color:'black'}}>{props.data.subdistrict.full_name}</Text>
                        <Text style={{color:'black'}}>{props.data.description}</Text>
                    </View>
                </TouchableOpacity>
            )
        }


const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
  
    },
    addressCard: {
        padding: 25,
        marginBottom: 10,
        borderRadius: 10,
        backgroundColor: '#393e46',
        borderWidth: 1,
        borderColor: 'white'
    },
    Text: {
        color: 'white',
    },
    delete: {
        width: '20%',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: 1
    },
    update: {
        width: '30%',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: 130
    }
    
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser,
        dataAddress: state.account.dataAddress,
        districts: state.account.districts,
        country: state.account.country,
        warehouse: state.orders.warehouse,
        dataSaveWarehouse: state.orders.dataSaveWarehouse,
        getWarehouse: state.orders.getWarehouse,
        
    }),
    dispatch => ({
        getWarehouse: (data,page) => dispatch(getWarehouse(data,page)),
        saveWarehouse: (data) => dispatch(saveWarehouse(data)),
        setWarehouse: (data) => dispatch(setWarehouse(data)),
    })
)
(ChooseWarehouseViews);

export default Account;