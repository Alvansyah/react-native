import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';
import {connect} from 'react-redux';
import {UpdateAddress} from './AccountState';
import { Actions } from 'react-native-router-flux';


class UpdateAddressView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            alias: this.props.address.alias,
            address: this.props.address.address,
            recipient: this.props.address.recipient_name,
            mobile_number: this.props.address.mobile_number,
            zip_code: this.props.address.zip_code
        };
    }

    componentDidMount(){
        
    }
    
    onUpdateAddress(){
        this.props.updateAddress(id,alias,address,recipient,mobile_number,country,zip_code);
        var id = this.props.address.id
        var alias = this.state.alias; 
        var address = this.state.address;
        var recipient = this.state.recipient;
        var mobile_number = this.state.mobile_number;
        var zip_code = this.state.zip_code;
        var country = this.props.address.subdistrict.id;

        
    }

    render(){
        return (
                <Container>
                    <Head title={'Update Address'} back={true} />
                        <View style={styles.container}>
                            <View style={styles.formCard}>
                                <Text style={styles.textlabel}>Insert Alias</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({alias: text})}
                                > 
                                    {this.props.address.alias} 
                                </TextInput>
                                <Text style={styles.textlabel}>Insert Address</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({address: text})}
                                >
                                    {this.props.address.address} 
                                </TextInput>
                                <Text style={styles.textlabel}>Insert Recipient Name</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({recipient: text})}
                                >
                                    {this.props.address.recipient_name} 
                                </TextInput>
                                <Text style={styles.textlabel}>Insert Mobile Number</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({mobile_number: text})}
                                >
                                    {this.props.address.mobile_number} 
                                </TextInput>
                                <Text style={styles.textlabel}>Insert Subdistrict</Text>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => Actions.changeAddress()}
                                >
                                    <Text style={{color:'#393e46', marginTop:7, marginBottom:7}}>{(this.props.country != null) ? this.props.country.full_name : ''}</Text>
                                    <View style={{borderBottomColor:'#393e46', borderBottomWidth: 1, marginBottom:20}} />
                                </TouchableOpacity>
                                <Text style={styles.textlabel}>Insert Zip Code</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({zip_code: text})}
                                >
                                    {this.props.address.zip_code} 
                                </TextInput>
                            </View>
                            <View style={styles.Button}> 
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.onUpdateAddress()}
                            >
                                <Text style={{color:'white', textAlign:'center'}}>Update</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                    <Footer account={true} />
                </Container>
                )
            }
        }

const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',
    },
    textlabel: {
        color:'#393e46', 
        fontSize: 15, 
        fontWeight: 'bold'
    },
    textInput: {
        color:'#393e46',
        height: 40, 
        borderColor: '#393e46', 
        borderBottomWidth: 1, 
        marginBottom: 20,
        width: "100%"
    },
    changename: {
        backgroundColor:'#fb8c00', 
        borderColor: 'white', 
        borderWidth: 1,
        borderRadius: 5
    },
    formCard: {
        backgroundColor:'#eeeeee', 
        borderColor: '#393e46', 
        borderWidth: 1,
        borderRadius: 5,
        padding: 25
    },
    Button: {
        marginTop: 20,
        backgroundColor:'#fb8c00', 
        borderColor: 'white', 
        borderWidth: 1,
        borderRadius: 5,
        padding: 10
    }
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser,
        country: state.account.country
    }),
    dispatch => ({
        updateAddress: (id,alias,address,recipient,mobile_number,country,zip_code) => dispatch(updateAddress(id,alias,address,recipient,mobile_number,country,zip_code)),
    })
)
(UpdateAddressView);

export default Account;