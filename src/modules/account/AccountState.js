// @flow
import Axios from '../../helpers/Axios'
import { PermissionsAndroid, ToastAndroid } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { getCurrentUser } from '../AppState';

//inisial state 
export const initialState = {
    loading: false,
    imageUser: null,
    districts: [],
    country: null,
    dataAddress: [],
    dataChoose: {}
};

//actions
export const SET_IMAGE = 'AppState/SET_IMAGE';
export const SET_LOADING = 'AppState/SET_LOADING';
export const SET_DISTRICS = 'AppState/SET_DISTRICS';
export const SET_COUNTRY = 'AppState/SET_COUNTRY';
export const SET_ADDRESS = 'AppState/SET_ADDRESS';
export const SET_SAVEDATACHOOSE = 'AppState/SET_SAVEDATACHOOSE';

//action creator
export function setLoading(loading){
    return {
        type: SET_LOADING,
        loading,
    };
}

export function imageAdd(uri) {
    return {
        type: SET_IMAGE,
        uri,
    };
}

export function setDistricts(address) {
    return {
        type: SET_DISTRICS,
        address,
    };
}


export function setCountry(country) {
    return {
        type: SET_COUNTRY,
        country,
    };
}

export function setAddress(data) {
    return {
        type: SET_ADDRESS,
        data,
    };
}


export function saveDataChoose(data) {
    return {
        type: SET_SAVEDATACHOOSE,
        data,
    };
}

export function dataChoose(dataChoose) {
    return {
        type: SET_SAVEDATACHOOSE,
        dataChoose,
    };
}


export function saveChoose(data){
    return (dispatch, getState) => {

        dispatch(setLoading(true));
        dispatch(saveDataChoose(data))
        Actions.pop()
    };
}

export function getVoucher(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let voucherRequest = {
            method: 'get',
            url: `/api/vouchers`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
        console.log('voucherRequest = ', voucherRequest);
    
        Axios(voucherRequest)
            .then(response => {
                const data = response.data.data;                
                
                dispatch(setAddress(data));
            })
            .catch(error => {

            });
    };
}

export function UpdateAddress(id,alias,address,recipient,mobile_number,country,zip_code){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let UpdateAddressRequest = {
            method: 'patch',
            url: `/api/user/addresses/${id}`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
            },
            data: {
                	"alias": `${alias}`,
                	"address": `${address}`,
                	"recipient_name": `${recipient}`,
                	"mobile_number": `${mobile_number}`,
                	"subdistrict_id": country,
                	"zip_code": `${zip_code}`
                }
        };
        console.log('UpdateAddressRequest = ', UpdateAddressRequest);
    
        Axios(UpdateAddressRequest)
            .then(response => {
                
                Actions.Account();
                console.log('Update Address Success')
                
            })
            .catch(error => {
                console.log(error)
                console.log('Update Address Failed')
            });
    };
}

export function delAddress(id){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let delAddressRequest = {
            method: 'delete',
            url: `/api/user/addresses/${id}`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
            },
        };
        console.log('delAddressRequest = ', delAddressRequest);
    
        Axios(delAddressRequest)
            .then(response => {
                ToastAndroid.show('Delete Success', ToastAndroid.SHORT);
            })
            .catch(error => {
                ToastAndroid.show('Delete Failed', ToastAndroid.SHORT);
            });
    };
}

export function getAddress(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let addressRequest = {
            method: 'get',
            url: `/api/user/addresses`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
            },
        };
        console.log('updateImageRequest = ', addressRequest);
    
        Axios(addressRequest)
            .then(response => {
                const data = response.data.data;                
                
                dispatch(setAddress(data));
            })
            .catch(error => {

            });
    };
}

export function updateImage(uri){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let updateImageRequest = {
            method: 'patch',
            url: `/api/user/update`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
            data: uri
        };
    
        Axios(updateImageRequest)
            .then(response => {
                
                dispatch(getCurrentUser());
                console.log('sukses post')
                
            })
            .catch(error => {
                console.log(error, 'gagal post image')
                
            });
    };
}

export function getDistricts(address,page){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let districtsRequest = {
            method: 'get',
            url: `/api/subdistricts`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
            params: {
                "keyword":`${address}`,
                "per_page":5,
                "page":page
            }
        };
    
        Axios(districtsRequest)
            .then(response => {
                const data = response.data.data;                
                
                dispatch(setDistricts(data));
            })
            .catch(error => {

            });
    };
}

export function dataDistricts(){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let districtsRequest = {
            method: 'get',
            url: `/api/subdistricts`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
        };
    
        Axios(districtsRequest)
            .then(response => {
                const data = response.data.data;                
                
                dispatch(setDistricts(data));
            })
            .catch(error => {

            });
    };
}

export function saveCountry(country){
    return (dispatch, getState) => {
        dispatch(setLoading(true));
        dispatch(setCountry(country));

    };
}

export function postAddress(alias,address,recipient,mobile_number,country,zip_code){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let postAddressRequest = {
            method: 'post',
            url: `/api/user/addresses`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
            },
            data: {
                	"alias": `${alias}`,
                	"address": `${address}`,
                	"recipient_name": `${recipient}`,
                	"mobile_number": `${mobile_number}`,
                	"subdistrict_id": country,
                	"zip_code": `${zip_code}`
                }
        };
    
        Axios(postAddressRequest)
            .then(response => {
                
                ToastAndroid.show('Address Success', ToastAndroid.SHORT);
                Actions.account();
                
            })
            .catch(error => {
                console.log(error, 'Address Failed')
                
            });
    };
}

export function updateName(data){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let updateImageRequest = {
            method: 'patch',
            url: `/api/user/update`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
            data: {name: data}
        };
    
        Axios(updateImageRequest)
            .then(response => {
                dispatch(getCurrentUser());
                ToastAndroid.show('Update Name Success', ToastAndroid.SHORT);
                Actions.account();
                
            })
            .catch(error => {
                
            });
    };
}


export function setNewPassword(oldpass,newpass,){
    return (dispatch, getState) => {
        const state = getState();
        var accessToken = state.app.accessToken
        dispatch(setLoading(true));

        let updatepassword = {
            method: 'patch',
            url: `/api/user/update_password`,
            headers: {
                'Authorization': `Bearer ${accessToken}`,
                "Content-Type": 'application/json',
                "Accept": 'application/json'
            },
            data: {
                "old_password": `${oldpass}`,
                "new_password": `${newpass}`
            }
        };
    
        Axios(updatepassword)
            .then(response => {
                
                dispatch(doLogout());
                ToastAndroid.show('Update Password Success', ToastAndroid.SHORT);
            })
            .catch(error => {
               
                ToastAndroid.show('Update Password Failed', ToastAndroid.SHORT);
            });
    };
}

export default function AccountStateReducer(state = initialState, action = {}) {
    switch (action.type) {
        case SET_LOADING:
            return {
                ...state,
                loading: action.loading,
            };
        case SET_IMAGE:
            return {
                ...state,
                uri: action.uri,
                loading:false
            };
        case SET_DISTRICS:
            return {
                ...state,
                districts: action.address,
                loading:false
            };
        case SET_COUNTRY:
            return {
                ...state,
                country: action.country,
                loading:false
            };
        case SET_ADDRESS:
            return {
                ...state,
                dataAddress: action.data,
                loading:false
            };
        case SET_SAVEDATACHOOSE:
            return {
                ...state,
                dataChoose: action.dataChoose,
                loading:false
            };
        default:
            return state;
    }
}