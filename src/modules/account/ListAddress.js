import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput,
    Alert
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';
import {connect} from 'react-redux';
import {getAddress,delAddress} from './AccountState';
import { Actions } from 'react-native-router-flux';


class listAddressView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 

        };
        this.deleteAddress = this.deleteAddress.bind(this)
    }

    componentDidMount(){
        this.props.getAddress()
    }

    deleteAddress(data){
        console.log('data', data)
        this.props.delAddress(data)
        Actions.ListAddress()
    }

    render(){
        return (
            <Container>
                <Head title={'List Address'} back={true} />
                    <View style={styles.container}>
                        <FlatList
                            data= {this.props.dataAddress}
                            bounces= {false}
                            numColumns= {1}
                            showsVerticalScrollIndicator={false}
                            keyExtractor= {(item, i) => `address-${i}`}
                            renderItem= {({item, index}) => {                                
                                return(
                                    <AddressCard data={item} deleteAdd={(data) => this.deleteAddress(data)} />
                                )
                            }}
                        />
                    </View>
                <Footer account={true} />
            </Container>
                )
            }
        }

        const AddressCard = (props) => {
            return (
                <View style={styles.addressCard}>
                    <Text style={[styles.Text,{fontWeight: 'bold', marginBottom: 15}]}>{props.data.alias}</Text>
                    <Text style={styles.Text}>{props.data.recipient_name}</Text>
                    <Text style={styles.Text}>{props.data.address}</Text>
                    <Text style={styles.Text}>{props.data.subdistrict.full_name}</Text>
                    <Text style={styles.Text}>{props.data.mobile_number}</Text>
                    <Text style={styles.Text}>{props.data.zip_code}</Text>
                    <View style={{borderTopColor: 'white', borderTopWidth:1, marginTop: 30}} />
                    <View style={{flexDirection: 'row'}}>
                        <TouchableOpacity
                            activeOpacity={0.8}
                            onPress={() => Actions.updateaddress({address: props.data.address})}
                            style={styles.update}
                        >
                            <Text style={{color: 'white', fontWeight: 'bold', }}>Update</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                            activeOpacity={0.8}
                            onPress={() => 
                                Alert.alert(
                                    'Alert',
                                    'Yakin Dhapus ?',
                                      [{
                                        text: 'Cancel',
                                        onPress: () => console.log('Cancel Pressed'),
                                      },
                                      {text: 'OK', onPress: () => props.deleteAdd(props.data.id)},
                                      ],
                                    {cancelable: false},
                                  )}
                            style={styles.delete}
                        >
                            <Text style={{color: 'red', fontWeight: 'bold', }}>Delete</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }


const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',
    },
    addressCard: {
        padding: 25,
        marginBottom: 10,
        borderRadius: 10,
        backgroundColor: '#9e9e9e',
        borderWidth: 1,
        borderColor: 'white'
    },
    Text: {
        color: 'white',
    },
    delete: {
        width: '20%',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: 1
    },
    update: {
        width: '30%',
        alignItems: 'center',
        marginTop: 10,
        marginLeft: 130
    }
    
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser,
        dataAddress: state.account.dataAddress
    }),
    dispatch => ({
        getAddress: () => dispatch(getAddress()),
        delAddress: (id) => dispatch(delAddress(id))
    })
)
(listAddressView);

export default Account;