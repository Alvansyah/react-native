import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';
import { Hoshi } from 'react-native-textinput-effects';
import {
    connect
} from 'react-redux';
import {
    postAddress
} from './AccountState';
import { Actions } from 'react-native-router-flux';


class formAddressView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            alias: null,
            address: null,
            recipient: null,
            mobile_number:null,
            zip_code:null
        };
    }


// {
// 	"alias": "habibi",
// 	"address": "Tenggilis Square M24 No. 1, Depan Kimia Farma",
// 	"recipient_name": "habibi",
// 	"mobile_number": "0934783247",
// 	"subdistrict_id": 2529,
// 	"zip_code": "60192"
// }

    componentDidMount(){

    }

    onAddress(){
        var alias = this.state.alias; 
        var address = this.state.address;
        var recipient = this.state.recipient;
        var mobile_number = this.state.mobile_number;
        var zip_code = this.state.zip_code;
        var country = this.props.country.id;

        this.props.postAddress(alias,address,recipient,mobile_number,country,zip_code);
        //console.log("addres", country);
        
    }

    render(){
        return (
                <Container>
                    <Head title={'Form Address'} back={true} />
                        <View style={styles.container}>
                            <View style={styles.formCard}>
                                <Text style={styles.textlabel}>Insert Alias</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({alias: text})}
                                />
                                <Text style={styles.textlabel}>Insert Address</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({address: text})}
                                />
                                <Text style={styles.textlabel}>Insert Recipient Name</Text>
                                <TextInput
                                    style={styles.textInput}
                                    onChangeText={text => this.setState({recipient: text})}
                                />
                                <Text style={styles.textlabel}>Insert Mobile Number</Text>
                                <TextInput
                                    style={styles.textInput}
                                    keyboardType={'numeric'}
                                    onChangeText={text => this.setState({mobile_number: text})}
                                />
                                <Text style={styles.textlabel}>Insert Subdistrict</Text>
                                <TouchableOpacity
                                    activeOpacity={1}
                                    onPress={() => Actions.changeaddress()}
                                >
                                    <Text style={{color:'#393e46', marginTop:7, marginBottom:7}}>{(this.props.country != null) ? this.props.country.full_name : ''}</Text>
                                    <View style={{borderBottomColor:'#393e46', borderBottomWidth: 1, marginBottom:20}} />
                                </TouchableOpacity>
                                <Text style={styles.textlabel}>Insert Zip Code</Text>
                                <TextInput
                                    style={styles.textInput}
                                    keyboardType={'numeric'}
                                    onChangeText={text => this.setState({zip_code: text})}
                                />
                            </View>
                            <View style={styles.Button}> 
                            <TouchableOpacity
                                activeOpacity={0.8}
                                onPress={() => this.onAddress()}
                                //style={{borderColor:'white', borderWidth:1}}
                            >
                                <Text style={{color:'white', textAlign:'center'}}>Submit</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                    <Footer account={true} />
                </Container>
                )
            }
        }

const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',
        //justifyContent: 'center',
        //alignItems: 'center',
    },
    textlabel: {
        color:'#393e46', 
        fontSize: 15, 
        fontWeight: 'bold'
    },
    textInput: {
        color:'#393e46',
        height: 40, 
        borderColor: '#393e46', 
        borderBottomWidth: 1, 
        marginBottom: 20,
        width: "100%"
    },
    changename: {
        backgroundColor:'#262424e6', 
        borderColor: 'white', 
        borderWidth: 1,
        borderRadius: 5
    },
    formCard: {
        backgroundColor:'#eeeeee', 
        borderColor: '#393e46', 
        borderWidth: 1,
        borderRadius: 5,
        padding: 25
    },
    Button: {
        marginTop: 20,
        backgroundColor:'#fb8c00', 
        borderColor: 'white', 
        borderWidth: 1,
        borderRadius: 5,
        padding: 10
    }
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser,
        country: state.account.country
    }),
    dispatch => ({
        updateName: (data) => dispatch(updateName(data)),
        postAddress: (alias,address,recipient,mobile_number,country,zip_code) => dispatch(postAddress(alias,address,recipient,mobile_number,country,zip_code)),
    })
)
(formAddressView);

export default Account;