import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput,
    ToastAndroid
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';
import {
    connect
} from 'react-redux';
import {
    updateName,
    getDistricts,
    dataDistricts,
    saveCountry
} from './AccountState';
import { Actions } from 'react-native-router-flux';


class changeAddressView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            country: null,
            address: null,
            page: 0,
        };
        this.setAddress = this.setAddress.bind(this)
        //this.props.saveCountry = this.props.saveCountry.bind(this)
    }

    componentDidMount(){
        //this.props.dataDistricts()
    }

    setAddress(data){
        this.setState({
            country: data.full_name
        })
        //console.log(this.state.country);
    }

    searchAddress(address,page){
        this.setState({
            address: address,
            page: page
        })

        this.props.getDistricts(this.state.address, this.state.page)
    }

    addPage(){
        this.setState({
            page: this.state.page + 1
        })

        this.props.getDistricts(this.state.address, this.state.page)
    }

    minPage(){
        if (this.state.page == 0) {
            return ToastAndroid.show('No Data Address', ToastAndroid.SHORT);
        }
        this.setState({
            page: this.state.page - 1
        })

        this.props.getDistricts(this.state.address, this.state.page)
    }

    render(){
        console.log(this.props.country);
        
        return (
            <Container>
                <Head title={'Select Address'}  />
                    <View style={styles.container}>
                        <View style={{alignSelf:'center', alignItems:'center'}}>
                            <Text style={{color: '#393e46', fontWeight: 'bold', fontSize: 16, marginBottom: 13, textAlign: 'center'}}>{(this.props.country.full_name) , 'Search Your Address'}</Text>
                        </View>
                        <View style={{flexDirection:'row', borderBottomColor: '#393e46', borderTopColor: '#393e46', borderBottomWidth: 1, borderTopWidth: 1, marginBottom: 20, width:'80%', alignItems: 'center', alignSelf: 'center'}}>
                            <Icon 
                                name='map-search-outline'
                                type='MaterialCommunityIcons'
                                style={{color: '#fb8c00 ', paddingRight: 10}}
                            />
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.searchAddress(text, 1)}
                                secureTextEntry={this.state.secureTextold}
                            />
                        </View>
                        <View>
                            <FlatList
                                data= {this.props.districts}
                                bounces= {false}
                                //horizontal={true}
                                numColumns= {1}
                                showsVerticalScrollIndicator={false}
                                //ListHeaderComponent={ () => this.head() }
                                keyExtractor= {item => `address-${item.id}`}
                                renderItem= {({item, index}) => <DistrictsCard data={item} setCountry={(country) => this.props.saveCountry(country)} />}
                            />
                        </View>
                        <View style={{flexDirection:'row', alignSelf:'center', marginTop:90}}>
                            <TouchableOpacity
                                style={[styles.Buttonpage,{marginRight:20}]}
                                activeOpacity={0.8}
                                onPress= {() => this.minPage()}
                            >
                                <View>
                                    <Text style={{color: "white", textAlign:'center'}}>Before</Text>
                                </View>
                            </TouchableOpacity>    
                            <TouchableOpacity
                                style={styles.Buttonpage}
                                activeOpacity={0.8}
                                onPress= {() => this.addPage()}
                            >
                                <View>
                                    <Text style={{color: "white", textAlign:'center'}}>Next</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity
                            style={styles.Button}
                            activeOpacity={0.8}
                            onPress= {() => Actions.pop()}
                        >
                            <View>
                                <Text style={{color: "white", textAlign:'center'}}>Done</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                <Footer account={true} />
            </Container>
            )
        }
    }

    const DistrictsCard = (props) => {
        return (
            <TouchableOpacity
                style={styles.districtCard}
                activeOpacity={0.8}
                onPress= {() => props.setCountry(props.data)}
            >
                <View>
                    <Text style={{color: "white"}}>{props.data.full_name}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    

const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',

    },
    textlabel: {
        color:'#f57c00', 
        fontSize: 15, 
        fontWeight: 'bold'
    },
    textInput: {
        color:'#393e46',
        height: 40, 
        borderColor: '#f57c00', 
        borderBottomWidth: 0, 
        width: "90%",
    },
    changename: {
        backgroundColor:'#f57c00', 
        borderColor: 'white', 
        borderWidth: 1,
        borderRadius: 5
    },
    districtCard: {
        flex:1,
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
        backgroundColor: '#f57c00',
        flexDirection:'row',
        borderWidth: 1,
        borderColor: '#f57c00'
    },
    Button: {
        padding: 10,
        borderRadius: 4,
        marginTop: 30,
        alignSelf: 'center',
        marginBottom: 10,
        backgroundColor: '#f57c00',
        flexDirection:'row',
        borderWidth: 1,
        borderColor: '#f57c00'
    },
    Buttonpage: {
        padding: 10,
        borderRadius: 4,
        marginTop: 30,
        marginBottom: 10,
        backgroundColor: '#f57c00',
        flexDirection:'row',
        borderWidth: 1,
        borderColor: '#f57c00'
    },
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser,
        districts: state.account.districts,
        country: state.account.country
    }),
    dispatch => ({
        updateName: (data) => dispatch(updateName(data)),
        getDistricts: (address,page) => dispatch(getDistricts(address,page)),
        dataDistricts: () => dispatch(dataDistricts()),
        saveCountry: (country) => dispatch(saveCountry(country))
    })
)
(changeAddressView);

export default Account;