import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    Alert,
    TextInput
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
import Head from '../../component/Head';
import Footer from '../../component/Footer';
import ImagePicker from 'react-native-image-picker';
import {connect} from 'react-redux';
import {updateImage,updateDate,} from './AccountState';
import { Actions } from 'react-native-router-flux';


const options={
    title: 'Select a Photos',
    takePhotoButtonTitle: 'Choose Camera',
    chooseFromLibraryButtonTitle:'Choose From Gallery'
}

class AccountView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            photo: null,
        };
    }

    upload=() => { 
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
              console.log('User cancelled image picker');
            }
            else if (response.error) {
              console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
              console.log('User tapped custom button: ', response.customButton);
            }
            else{
                const source = { 
                    profile_picture_path: 'data:image/jpeg;base64,' + response.data 
                };
                this.props.imageUpload(source)
            }
        });
    }
 

    render(){
        return (
            <Container>
                <Head title={'Account'} />
                <View style={styles.container}>
                    <View style={styles.accountCard}>
                    <TouchableOpacity
                                    onPress={this.upload}>
                                    <Thumbnail 
                                    source={{uri:this.props.currentUser.data.profile_picture_path}} 
                                    style={[styles.img1]}
                            />
                                </TouchableOpacity>
                                <Icon name="user" 
                                    type="EvilIcons"
                                    onPress={Actions.changename}
                                    />
                                <View>
                                <TouchableOpacity>
                                    <Text style={[styles.textBody]}>{this.props.currentUser.data.name} </Text>
                                </TouchableOpacity>
                                    <Text style={[styles.textBody]}>{this.props.currentUser.data.mobile_number}</Text>
                                    <Text style={[styles.textBody]}>{this.props.currentUser.data.last_login}</Text>
                            <View style={{marginTop:15, marginLeft: 5, marginRight: 0}}>
                               <TouchableOpacity style={{backgroundColor:'green',margin:10, padding:10, borderRadius:10,marginLeft:20,marginTop:1}}
                                    onPress={Actions.changepassword}>
                                    <Text style={{color:'#fff',textAlign:'right'}}>Change Password</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => Actions.listaddress()}
                    >
                        <View style={styles.card}>
                            <Text style={{color:'white', paddingTop: 5}}>List Address</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => Actions.formaddress()}
                    >
                        <View style={styles.card}>
                            <Text style={{color:'white', paddingTop: 5}}>Add Your Address</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <Footer account={true} />
            </Container>
                )
            }
        }

const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',
    },
    colom: {
        flex:2,
    },
    flex: {
        flex:1,
    },
    textBody: {
        color: 'white',
        fontSize: 15,
        paddingHorizontal: 10,
        paddingVertical: 3,
    },
    imageAccount:{
        backgroundColor:'white',
        borderRadius:50,
        marginRight:10
    },
    accountCard: {
        padding: 25,
        borderRadius: 10,
        backgroundColor: '#2196f3',
        flexDirection:'row',
        borderWidth: 1,
        borderColor: 'white'
    },
    card: {
        marginTop: 17,
        padding: 20,
        borderRadius: 10,
        backgroundColor: '#2196f3',
        flexDirection:'row',
        borderWidth: 1,
        borderColor: 'white'
    },
    changePassword: {
        padding: 0,
        borderRadius: 5,
        marginRight:80,
        marginLeft:2,
        //width: '100%',
        backgroundColor: '#393e46',
        flexDirection:'row',
        borderWidth: 1,
        justifyContent: 'center',
        borderColor: 'white'
    },
    img1:{
        height:100,
        width:100,
        borderRadius:50,
        marginTop:1,        
    },
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser
    }),
    dispatch => ({
        updateImage: (uri) => dispatch(updateImage(uri)),
        updateDate: (data) => dispatch(updateDate(data))
    })
)
(AccountView);

export default Account;