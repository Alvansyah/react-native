import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';
//import PhotoUpload from 'react-native-photo-upload'
import ImagePicker from 'react-native-image-picker';
import {
    connect
} from 'react-redux';
import {
    setNewPassword
} from './AccountState';
import { Actions } from 'react-native-router-flux';


class changePasswordView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            passwordold: null,
            passwordnew: null,
            confirmation: null,
            visibleold: false,
            visiblenew: false,
            visibleconfirm: false,
            secureTextold: true,
            secureTextnew: true,
            secureTextconfirm: true
        };
    }

    componentDidMount(){
        var old = "old";
    }

    showPassword(password){
        if(password == 'old'){
            this.setState({
                visibleold : true,
                secureTextold : false
            })
        }
        if(password == 'new'){
            this.setState({
                visiblenew : true,
                secureTextnew : false
            })
        }
        if(password == 'confirm'){
            this.setState({
                visibleconfirm : true,
                secureTextconfirm : false
            })
        }
    }

    hidePassword(password){
        if (password == 'old'){
            this.setState({
                visibleold : false,
                secureTextold: true
            })
        }
        if (password == 'new'){
            this.setState({
                visiblenew : false,
                secureTextnew: true
            })
        }
        if (password == 'confirm'){
            this.setState({
                visibleconfirm : false,
                secureTextconfirm: true
            })
        }
    }

    render(){
        return (
            <Container>
                <Head title={'Change Password'} back={true} />
                    <View style={styles.container}>
                        <Text style={styles.textlabel}>Insert Old Password</Text>
                        <View style={{flexDirection:'row', borderBottomColor: '#222831', borderBottomWidth: 1, marginBottom: 10}}>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.setState({passwordold:text})}
                                secureTextEntry={this.state.secureTextold}
                            />
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => (this.state.visibleold == false) ? this.showPassword('old') : this.hidePassword('old')}
                                style={{flexDirection: 'row'}}
                            >
                                {
                                (this.state.visibleold == false) ?
                                    <Icon 
                                        name='ios-eye-off'
                                        type='Ionicons'
                                        style={{color: '#222831'}}
                                    />
                                    :
                                    <Icon 
                                        name='ios-eye'
                                        type='Ionicons'
                                        style={{color: '#222831'}}
                                    />
                                }
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.textlabel}>Insert New Password</Text>
                        <View style={{flexDirection:'row', borderBottomColor: '#222831', borderBottomWidth: 1, marginBottom: 10}}>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.setState({passwordnew: text})}
                                secureTextEntry={this.state.secureTextnew}
                            />
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => (this.state.visiblenew == false) ? this.showPassword('new') : this.hidePassword('new')}
                                style={{flexDirection: 'row'}}
                            >
                                {
                                (this.state.visiblenew == false) ?
                                    <Icon 
                                        name='ios-eye-off'
                                        type='Ionicons'
                                        style={{color: '#222831'}}
                                    />
                                    :
                                    <Icon 
                                        name='ios-eye'
                                        type='Ionicons'
                                        style={{color: '#222831'}}
                                    />
                                }
                            </TouchableOpacity>
                        </View>
                        <Text style={styles.textlabel}>Confirmation New Password</Text>
                        <View style={{flexDirection:'row', borderBottomColor: '#222831', borderBottomWidth: 1, marginBottom: 20}}>
                        <TextInput
                                style={styles.textInput}
                                onChangeText={text => this.setState({confirmation: text})}
                                secureTextEntry={this.state.secureTextconfirm}
                            />
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => (this.state.visibleconfirm == false) ? this.showPassword('confirm') : this.hidePassword('confirm')}
                                style={{flexDirection: 'row'}}
                            >
                                {
                                (this.state.visibleconfirm == false) ?
                                    <Icon 
                                        name='ios-eye-off'
                                        type='Ionicons'
                                        style={{color: '#222831'}}
                                    />
                                    :
                                    <Icon 
                                        name='ios-eye'
                                        type='Ionicons'
                                        style={{color: '#222831'}}
                                    />
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={styles.changepassword}>
                            <TouchableOpacity
                                activeOpacity={1}
                                onPress={() => this.props.setNewPassword(this.state.passwordold, this.state.passwordnew, this.state.confirmation)}
                            >
                                <Text style={{color: 'white', textAlign: 'center', paddingHorizontal: 10, paddingVertical: 10}}>Change Password</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                <Footer account={true} />
            </Container>
                )
            }
        }

const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',
        //
    },
    textlabel: {
        color:'#222831', 
        fontSize: 15, 
        fontWeight: 'bold',
    },
    textInput: {
        color:'#222831',
        height: 40, 
        borderBottomColor: '#222831', 
        borderBottomWidth: 0, 
        width: "90%",
    },
    changepassword: {
        backgroundColor:'#fb8c00', 
        borderColor: '#393e46', 
        borderWidth: 1,
        borderRadius: 5,
    }
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser
    }),
    dispatch => ({
        imageAdd: (uri) => dispatch(imageAdd(uri)),
        setNewPassword: (old,newpass,confirm) => dispatch(setNewPassword(old,newpass,confirm)),
    })
)
(changePasswordView);

export default Account;