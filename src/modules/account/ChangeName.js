import React, { Component } from 'react';
import {
    Text,
    StyleSheet,
    FlatList,
    TouchableOpacity,
    TextInput
} from 'react-native';
import { 
    Container, 
    Header, 
    Title, 
    Content, 
    FooterTab, 
    Button, 
    Left, 
    Right, 
    Body, 
    Icon,
    Thumbnail,
    Image,
    Text as nativeText,
    View} from 'native-base';
    import Head from '../../component/Head';
    import Footer from '../../component/Footer';;
import { connect} from 'react-redux';
import {updateName} from './AccountState';


class changeNameView  extends Component{

    constructor(props) { 
        super(props); 
        this.state = { 
            Name: null,    
        };
    }


    render(){
        return (
            <Container>
                <Head Title={'Name'} back={true} />
                    <View style={styles.container}>
                    <Text style={styles.textlabel}>Insert New Name</Text>
                        <TextInput
                            style={styles.textInput}
                            onChangeText={text => this.setState({Name: text})}
                        >
                            {this.props.nameuser}
                        </TextInput>
                        <View style={styles.changename}>
                            <TouchableOpacity 
                                activeOpacity={0.8}
                                onPress={() => this.props.updateName(this.state.Name)}
                            >
                                <Text style={{color: 'white', textAlign: 'center', paddingHorizontal: 10, paddingVertical: 10}}>Change Name</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                <Footer account={true} />
            </Container>
                )
            }
        }

const styles = StyleSheet.create({
    container : {
        flex: 1,
        padding: 20,
        backgroundColor: '#eeeeee',
    },
    textlabel: {
        color:'#222831', 
        fontSize: 15, 
        fontWeight: 'bold'
    },
    textInput: {
        color:'#222831',
        height: 40, 
        borderColor: '#222831', 
        borderBottomWidth: 1, 
        marginBottom: 20,
        width: "100%"
    },
    changename: {
        backgroundColor:'#393e46', 
        borderColor: '#393e46', 
        borderWidth: 1,
        borderRadius: 5
    }
})

const Account = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        currentUser: state.app.currentUser,
        imageUser: state.account.imageUser
    }),
    dispatch => ({
        updateName: (data) => dispatch(updateName(data)),
    })
)
(changeNameView);

export default Account;