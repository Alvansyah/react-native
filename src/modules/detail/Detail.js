import React, { Component} from "react";
import {Text, 
    View, 
    TouchableOpacity, 
    StyleSheet,
    Image,
    ActivityIndicator,
    FlatList,
    alert,
    TextInput
} from 'react-native'
import { Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title,
    Content,
    FooterTab,     
Thumbnail,
    Footer,
    Card,
    CardItem
    } from 'native-base';
import Head from '../../component/Head';
import {connect} from 'react-redux'
import {detail} from '../home/HomeState'
import NumberFormat from 'react-number-format';
import Modal from 'react-native-modal';
import {addToCart }from '../cart/CartState'
import { Actions } from "react-native-router-flux";



class DetailView extends Component{

    constructor(props){
        super(props)
        this.load=this.load.bind(this);
        this.state = {
            isVisible: false,
            quantity : 1,
            notes : ""
        };
        
    }

    toggleModal = () => {
        this.setState({ isVisible: !this.state.isVisible });
    };

    onAdd(){
        this.props.addToCart(parseInt(this.state.quantity, 10 ), this.props.product, this.state.notes)
    }
    
    componentDidMount(){
        this.load();
    }

    load(){
        this.props.detail(this.props.productid);
    }

    
    render() {     
        return (
            <Container >
                 <Head title={'Detail Product'} back={true}/>
                    <View style={styles.container}>
                        <View>
                            <Card>
                                <CardItem cardBody>
                                    <Image source={{ uri: this.props.product.image_path}} style={{height: 200, width: null, flex: 1}}/>
                                </CardItem>
                          
                                    <Body style={{alignItems:'center'}}>
                                        <Text style={{fontWeight:'bold', fontSize:20}}>{this.props.product.name}</Text>
                                        {/* <Text style={{fontWeight:'bold', fontSize:18}}>{this.props.product.category.name}</Text> */}
                                        <NumberFormat
                                            value={this.props.product.master_price}
                                            displayType={'text'}
                                            thousandSeparator={true}
                                            prefix={'Rp. '}
                                            renderText={value => <Text style={{fontWeight:'bold', color:'red', fontSize:25}}>{value}</Text>}
                                        />
                                    </Body>
                            </Card>
                        </View>
                    </View>
                <Footer style={{backgroundColor:'#fb8c00'}}>
                    <TouchableOpacity
                        style={styles.productCard}
                        activeOpacity={0.6}
                        onPress={this.toggleModal}
                          
                        >
                        <View style={styles.flex}> 
                            <Text style={{fontSize: 15, color:'white',fontWeight:'bold' }}>Add To Cart</Text>
                        </View>
                        <View>
                            <Modal
                                isVisible={this.state.isVisible}
                                animationType={"fade"}
                                transparent={true}   
                                onBackdropPress={() => this.setState({ isVisible: false })}     
                                style={styles.modalview}     
                                swipeDirection={['up', 'left', 'right', 'down']} 
                                onSwipeComplete={this.close}  
                                backdropColor="#B4B3DB"
                                backdropOpacity={0.8}
                                animationIn="zoomInDown"
                                animationOut="zoomOutUp"
                                animationInTiming={600}
                                animationOutTiming={600}
                                backdropTransitionInTiming={600}
                                backdropTransitionOutTiming={600}
                            >
                                <View style={{backgroundColor:'white'}}>
                                    <View style={{marginHorizontal:20, marginVertical:20}}>
                                    <Image source={{ uri: this.props.product.image_path}} style={{height: 200, width: "100%", borderWidth:1, borderRadius:10}}/>
                                    <Text style={{fontWeight:'bold', fontSize:20,textAlign:'center'}}>{this.props.product.name}</Text>
                                    <NumberFormat
                                            value={this.props.product.master_price}
                                            displayType={'text'}
                                            thousandSeparator={true}
                                            prefix={'Rp. '}
                                            renderText={value => <Text style={{textAlign:'center',fontWeight:'bold', color:'red', fontSize:25}}>{value}</Text>}
                                    />
                                    <TextInput 
                                    underlineColorAndroid='transparent'
                                    placeholder="Masukan Quantity"
                                    style={styles.TextInputStyle}
                                    keyboardType={'numeric'}
                                    value={this.state.quantity}
                                    onChangeText={text => this.setState({quantity: text})}
                                    />
                                    <TextInput 
                                    underlineColorAndroid='transparent'
                                    placeholder="Masukan Notes"
                                    style={styles.TextInputStyle}
                                    value={this.state.notes}
                                    onChangeText={text => this.setState({notes: text})}
                                    />
                                    <TouchableOpacity
                                    activeOpacity={0.5}
                                    onPress={()=>this.onAdd()}
                                    >
                                        <View style={[styles.addtocart]}>
                                            <Text style={{fontSize:20,paddingLeft:5,marginBottom:20,color:'white'}}>Masukan Keranjang</Text>
                                        </View>
                                    </TouchableOpacity>
                                    </View>
                                </View>    
                            </Modal>    
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={[styles.productCard]}
                        activeOpacity={0.7}
                        >                        
                        <View style={styles.flex}>
                            <Text style={{fontSize: 15, color:'white',fontWeight:'bold' }}>Beli</Text>
                        </View>
                    </TouchableOpacity>
                </Footer>
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:10,
        backgroundColor:'white'
    },
    flex: {
        flex:1,
        justifyContent:'center',
    },
    imageProduct:{
        width:'100%', 
        height: 150, 
        borderRadius:5, 
        marginBottom:10, 
        marginTop:10,
    },
    productCard: {
        flex:1,
        alignItems:'center',
        borderRadius: 10,
        marginBottom: 10,
        marginLeft: 10,
        marginTop: 10,
        marginRight: 10,
        borderColor: 'black'
    },
    Footer: {
        backgroundColor: 'red'
    },
    addtocart:{
        justifyContent:'center',
        backgroundColor:'#6ee45a',
        alignItems:'center',
        paddingTop:15,
        borderRadius:10
    },
    modalview:{
        justifyContent:'flex-end',
        margin:0
    },
    TextInputStyle:{
        borderWidth:1,
        borderRadius:10,
        marginBottom:5
    }

})

const detailProducts = connect(
    state => ({
        product:state.home.product
    }),
    dispatch => ({
        detail: (id) => dispatch(detail(id)),
        addToCart: (quantity, product, notes) => dispatch(addToCart(quantity, product,notes)),

    }),
)
(DetailView);


export default detailProducts;