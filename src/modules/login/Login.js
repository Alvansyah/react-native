import React, { Component} from "react";
import {Text, 
    View, 
    TouchableOpacity, 
    StyleSheet,
    TextInput} from 'react-native'
import { Actions } from "react-native-router-flux";
import { Container, 
    Header, 
    Left, 
    Body, 
    Right, 
    Button, 
    Icon, 
    Title} from 'native-base';
import Head from '../../component/Head'
import Axios from '../../helpers/Axios'
import {connect} from 'react-redux'
import {checkLogin,
        setLoginLoading} from '../AppState'


class Loginview extends Component{
    
    constructor(props) {
        super(props)
        this.state = {
            email : '',
            password:''
         }
    }

    onLogin(){
        this.props.checkLogin({
            username: this.state.email,
            password: this.state.password
        })
        if(this.props.isLogin == true) {
            Actions.home();
        }
    }


    render(){
        return(
            <Container style={{backgroundColor:'blue'}}>
                <Head title={'Login'}/>
                <View style={Styles.Container}>
                <View style={Styles.IconApple}>
                            <Icon
                                name="app-store"
                                type="Entypo"
                                style={Styles.Icons}
                            />
                        </View>
                    <View style={Styles.textInputContainer}>
                        <TextInput
                            style={Styles.textInput}
                            value={this.state.email}
                            placeholder="Masukan Email"
                            onChangeText={(text) => this.setState({email:text})}
                        />
                    </View>
                    <View style={Styles.textInputContainer}>
                        <TextInput
                            style={Styles.textInput}
                            value={this.state.password}
                            placeholder="Masukan Password"
                            onChangeText={(text) => this.setState({password:text})}
                            secureTextEntry={true}
                        />
                    </View>
                    <TouchableOpacity
                        style={Styles.button}
                        onPress={() => this.onLogin()}
                    > 
                        <Text style={Styles.buttonText}>Login</Text>
                        <View style={Styles.containerIcon}>
                            <Icon
                                name="caretright"
                                type="AntDesign"
                                style={Styles.icon}
                            />
                        </View>
                    </TouchableOpacity>
                </View>
            </Container>
            
        )
    }
}

const Styles = StyleSheet.create({
    Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding:30,
        backgroundColor:'white'
    },
    button:{
        paddingVertical:10,
        paddingHorizontal:20,
        borderRadius:5,
        backgroundColor:'#6ee45a',
        flexDirection:'row',
        alignItems:'center',
    },
    buttonText:{
        color: '#000000',
        textAlign:'center'
    },
    containerIcon:{
        justifyContent: 'center',
        marginLeft:5
    },
    icon :{
        color:'#000000',
        fontSize :20
    },
    textInputContainer:{
        paddingHorizontal:10,
        borderColor: '#357373',
        borderWidth:1,
        borderRadius:15,
        width:'100%',
        marginBottom:20
    },
    textInput:{
        width:'100%'
    },
    IconApple:{
       marginLeft: 5,
       justifyContent:'center'
    },
    Icons:{
        fontSize: 100,
        paddingVertical: 5 ,
        paddingHorizontal:10,
        marginBottom:20,        
    }
})

const Login = connect(
    state => ({
        loginLoading: state.app.loginLoading,
        isLogin: state.app.isLogin,
    }),
    dispatch => ({
        setLoginLoading: (value) => dispatch(setLoginLoading(value)),
        checkLogin: (value) => dispatch(checkLogin(value)),
    }),
)
(Loginview)

export default Login;