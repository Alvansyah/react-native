import React,{ Component} from 'react';
import AppRouter from './Router'
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/es/integration/react'
import {store,persistor} from './redux/Store'
import {Text} from 'react-native'


export default class App extends Component{
    constructor(props){
        super(props);
        console.disableYellowBox = true;
        this.state ={
        };
    }
    render(){
        return (
            <Provider store={store}>
                {/* <--Provider : menghubungkan biar anak anakna bisa konek dan akses --/> */}
                <PersistGate 
                // PersistGate : biar state nya tidak di riset stiap buka aplikasi ,,, nyimpen session
                // fiktur login
                        loading={
                            <Text>Loading</Text>
                        }
                        persistor={persistor}
                    >
                        <AppRouter/>
                </PersistGate>
            </Provider>
        );
    }
}
 