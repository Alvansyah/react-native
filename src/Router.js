import {Router,Stack,Scene} from 'react-native-router-flux'
import Login from './modules/login/Login'
import Home from './modules/home/Home'
import React, { Component} from "react";
import {connect} from 'react-redux'
import Account from './modules/account/Account'
import Detail from './modules/detail/Detail'
import Cart from './modules/cart/Cart'
import Orders from './modules/orders/Orders'
import ChangeName from './modules/account/ChangeName'
import ChangePassword from './modules/account/ChangePassword';
import ChangeAddress from './modules/account/ChangeAddress';
import FormAddress from './modules/account/FormAddreess';
import ListAddress from './modules/account/ListAddress';
import UpdateAddress from './modules/account/UpdateAddress';
import chooseAddress from './modules/orders/chooseAddress';
import ChooseWarehouse from './modules/orders/ChooseWarehouse';




class Routerview extends Component{
  render(){
    return(
      <Router>
          <Stack key="root">
                <Scene key="login" initial={!this.props.isLogin} component={Login} hideNavBar={true}/>
                <Scene key="home" initial={this.props.isLogin} component={Home} hideNavBar={true} />
                <Scene key="detail" component={Detail} hideNavBar={true} />
                <Scene key="account" component={Account} hideNavBar={true} />
                <Scene key="cart" component={Cart} hideNavBar={true} />
                <Scene key="orders" component={Orders} hideNavBar={true} />
                <Scene key="changepassword" component={ChangePassword} hideNavBar={true} />
                <Scene key="changename" component={ChangeName} hideNavBar={true} />
                <Scene key="changeaddress" component={ChangeAddress} hideNavBar={true} />
                <Scene key="listaddress" component={ListAddress} hideNavBar={true} />
                <Scene key="formaddress" component={FormAddress} hideNavBar={true} />
                <Scene key="updateaddress" component={UpdateAddress} hideNavBar={true} />
                <Scene key="chooseAddress" component={chooseAddress} hideNavBar={true} />
                <Scene key="choosewarehouse" component={ChooseWarehouse} hideNavBar={true} />
          </Stack>
    </Router>
    )
  }
  
}

const RouterApp = connect(
  state => ({
      isLogin: state.app.isLogin,
  }),
)
(Routerview)

  export default RouterApp;