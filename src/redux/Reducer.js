import {combineReducers} from 'redux';
import app from '../modules/AppState';
import home from '../modules/home/HomeState';
import cart from '../modules/cart/CartState';
import orders from '../modules/orders/OrdersState';
import account from '../modules/account/AccountState';

export default combineReducers({
    app,
    home,
    cart,
    orders,
    account,
});
